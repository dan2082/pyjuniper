#!/usr/bin/python

import os
import logging
import sys
from xml.etree import ElementTree as ET

def load():
	os.system('launchctl load -w /Library/LaunchAgents/net.juniper.pulsetray.plist')
def unload():
	os.system('launchctl unload -w /Library/LaunchAgents/net.juniper.pulsetray.plist')

class Config(object):
	file = None
	is_loaded = None
	def __init__(self, file):
		self.file = file
		self.is_loaded = None
		directory = os.path.dirname(self.file)
		try:
			if os.path.isdir(directory) is False: os.mkdir(directory)
		except OSError as e:
			if e.errno == 13: exit(e)
			else: logging.error(e)
		try:
			element_tree = ET.parse(self.file)
			element = element_tree.getroot()
			try: self.is_loaded = bool(element.attrib['is_loaded'])
			except KeyError as e: logging.error(e)
		except IOError as e: logging.error(e)
	def save(self):
		try:
			element = ET.fromstring('<root />')
			if self.is_loaded: element.attrib['is_loaded'] = str(self.is_loaded)
			element_tree = ET.ElementTree(element)
			element_tree.write(file, encoding='UTF-8', xml_declaration=True, method='xml')
		except IOError as e: exit(e)

if __name__ == '__main__':
	level = logging.CRITICAL
	stream = sys.stdout
	logging.basicConfig(level=level, stream=stream)
	
	file = os.path.join(os.getenv('HOME'), '.juniper', 'config.xml')
	config = Config(file)
	if config.is_loaded: unload()
	else: load()
	config.is_loaded = not config.is_loaded
	config.save()
