# README #

I got tired of having the Juniper client autostart on my mac, so I created this script to load/unload the client.

### What is this repository for? ###

* A script to load/unload the juniper client
* Version: 1.0

### How do I get set up? ###

* Summary of set up
> Everything is setup within the script
* Configuration
> You will need write access to your home directory to create a hidden directory and file
* Dependencies
> Python 2.7

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* https://bitbucket.org/dan2082/pyjuniper/issues?status=new&status=open